package com.vs.androidflatui.enma.flatdesign;




import com.vs.androidflatui.enma.adapters.MyColorArrayAdapter;
import com.vs.androidflatui.enma.utils.Utils;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener,OnItemClickListener {

	// 
	ImageButton btnSettings,btnSearch;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ViewGroup vg = (ViewGroup) findViewById(R.id.root);
		Utils.setFontAllView(vg);

		
		btnSearch = (ImageButton) findViewById(R.id.btnImg_Search);
		btnSettings = (ImageButton) findViewById(R.id.btnImg_Settings);
		
		btnSearch.setOnClickListener(this);
		btnSettings.setOnClickListener(this);
		
		ListView listView = (ListView) findViewById(R.id.list_colors);
		String[] values = new String[] { "V chat", "Twitter", "FaceBook", "Youtube",
				"FaceBook", "FaceBook", "Traning" };
		Integer[] images = { R.drawable.s5, R.drawable.s2,
				R.drawable.s1, R.drawable.s3,
				R.drawable.s6, R.drawable.s10,
				R.drawable.s8 };

		// Create Color Adapter
		MyColorArrayAdapter adapter = new MyColorArrayAdapter(this, values,
				images);

		// Assign adapter to ListView
		listView.setAdapter(adapter);

		// Add Listener to handle click on list row
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnImg_Search){
			Intent intent = new Intent(this, SearchActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		}
		if (v.getId() == R.id.btnImg_Settings){
			Intent intent = new Intent(this, SettingActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
		}

	}
	
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Intent intent = new Intent(this, SubActivity.class);
		intent.putExtra("id", v.getId());
		startActivity(intent);
		overridePendingTransition(R.anim.zoom_out, R.anim.fade_out);
	}

}
